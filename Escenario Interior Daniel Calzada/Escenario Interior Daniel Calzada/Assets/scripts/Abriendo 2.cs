using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo2 : MonoBehaviour
{
    public Animator Door_Animation2;

    private void OnTriggerEnter(Collider other)
    {
        Door_Animation2.Play("Door Open");
    }
    private void OnTriggerExit(Collider other)
    {
        Door_Animation2.Play("Door Close 2");
    }

}
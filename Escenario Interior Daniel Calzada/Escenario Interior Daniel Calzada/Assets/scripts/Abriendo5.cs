using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo5 : MonoBehaviour
{
    public Animator Door_Girls_Animation;

    private void OnTriggerEnter(Collider other)
    {
        Door_Girls_Animation.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        Door_Girls_Animation.Play("Close");
    }

}
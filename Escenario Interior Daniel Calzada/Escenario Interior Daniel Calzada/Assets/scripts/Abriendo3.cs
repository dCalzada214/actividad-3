using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo3 : MonoBehaviour
{
    public Animator Door_Boys_Animation;

    private void OnTriggerEnter(Collider other)
    {
        Door_Boys_Animation.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        Door_Boys_Animation.Play("Close");
    }

}
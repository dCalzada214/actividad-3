using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo6 : MonoBehaviour
{
    public Animator Door_Frigorifico_Animation;

    private void OnTriggerEnter(Collider other)
    {
        Door_Frigorifico_Animation.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        Door_Frigorifico_Animation.Play("Close");
    }

}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo1 : MonoBehaviour
{
    public Animator Door_Animation;

    private void OnTriggerEnter(Collider other)
    {
        Door_Animation.Play("Door_Opern");
    }
    private void OnTriggerExit(Collider other)
    {
        Door_Animation.Play("Door Close");
    }

}
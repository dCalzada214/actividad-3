using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo4 : MonoBehaviour
{
    public Animator Door_Bboys_Animation;

    private void OnTriggerEnter(Collider other)
    {
        Door_Bboys_Animation.Play("open");
    }
    private void OnTriggerExit(Collider other)
    {
        Door_Bboys_Animation.Play("Close");
    }

}